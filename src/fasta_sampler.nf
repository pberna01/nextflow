
Channel
  .fromPath( "data/tiny_dataset/fasta/*.fasta" )
  .set { fasta_file }

process sample_fasta {
  input:
file fasta from fasta_file

publishDir "results/sampling/", mode: 'copy'

output:
file "*_sample.fasta" into fasta_sample

  script:
"""
head ${fasta} > ${fasta.baseName}_sample.fasta
"""
}